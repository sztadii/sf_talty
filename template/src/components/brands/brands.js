var brands = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $('.brands');
    $slider = $el.find('.brands__slider');

    sliderMake();
  };

  var sliderMake = function () {
    if ($slider.length > 0) {
      $slider.slick({
        dots: false,
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        fade: false,
        adaptiveHeight: true,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 1500,
        draggable: false,
        pauseOnHover: true,
        pauseOnFocus: false,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        ]
      });
    }
  };
};