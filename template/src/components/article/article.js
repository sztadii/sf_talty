var article = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  function init() {
    $el = $('.article');
    $slider = $el.find('.article__slider');

    if ($slider.length > 0) {
      $slider.imagesLoaded().done(function () {

        $slider.slick({
          infinite: false,
          dots: false,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: false,
          adaptiveHeight: true,
          autoplay: true,
          speed: 1000,
          autoplaySpeed: 1500,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });
      });
    }
  }
};