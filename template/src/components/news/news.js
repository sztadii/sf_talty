var news = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $('.news');
    $slider = $el.find('.news__slider');

    if ($slider.length > 0) {
      $slider.imagesLoaded().done(function () {

        $slider.slick({
          //TOCHECK INFINITY

          //infinite: false,
          dots: false,
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          fade: false,
          adaptiveHeight: true,
          autoplay: true,
          speed: 1000,
          autoplaySpeed: 1500,
          draggable: false,
          pauseOnHover: true,
          pauseOnFocus: false,
          responsive: [{
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }, {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          ]
        });
      });
    }
  };
};