var menu = new function () {

  //private functions
  var menuBreakpoint = 1024;

  //catch DOM
  var $windowWidth;
  var $menuButton;
  var $menuBackground;
  var $menuListMain;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    setVars();
    triggerMenu();
  };

  var setVars = function () {
    $windowWidth = $(window).width();
    $menuButton = $('.menu__button');
    $menuBackground = $('.menu .menu__background');
    $menuListMain = $('.menu .menu__list.-main');
  };

  var triggerMenu = function () {
    $menuButton.on('click', function () {
      toggleMenu();
    });

    $menuBackground.on('click', function () {
      toggleMenu();
    });

    $('.menu__link').on('click', function () {
      toggleMenu();
    });
  };

  var hideMenu = function () {
    $menuBackground.fadeOut(500);
    $menuListMain.slideUp(400, function () {
      $menuButton.removeClass('-active');
    });
  };

  var showMenu = function () {
    $menuBackground.fadeIn(500);
    $menuListMain.slideDown(400, function () {
      $menuButton.addClass('-active');
    });
  };

  var toggleMenu = function () {
    if (menuBreakpoint > $windowWidth) {
      if ($menuButton.hasClass('-active')) {
        hideMenu();
      } else {
        showMenu();
      }
    }
  };
};