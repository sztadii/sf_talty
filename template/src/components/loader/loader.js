var loader = new function () {

  //catch DOM
  var $el;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".loader");

    setTimeout(function () {
      $el.addClass('-hide');

      setTimeout(function () {
        $el.remove();
      }, 2000);
    }, 500);
  };
};


