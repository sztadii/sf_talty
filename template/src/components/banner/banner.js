var banner = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $('.banner.-main');
    $slider = $el.find('.banner__slider');

    sliderMake();
  };

  var sliderMake = function () {
    if ($slider.length > 0) {
      $slider.slick({
        infinite: false,
        dots: false,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: false,
        adaptiveHeight: true,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 1500,
        draggable: false,
        pauseOnHover: false,
        pauseOnFocus: false,
        vertical: true
      });
    }
  };
};
