var offer = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $('.offer');
    $slider = $el.find('.offer__slider');

    sliderMake();
  };

  var sliderMake = function () {
    if ($slider.length > 0) {
      $slider.each(function () {
        $(this).slick({
          infinite: false,
          dots: true,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: false,
          adaptiveHeight: true,
          autoplay: true,
          speed: 1000,
          autoplaySpeed: 1500,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false,
          customPaging: function (a, b) {
            return '<div class="slick-dot"></div>';
          },
          appendDots: $(this).parent().parent().find('.offer__dots'),
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
      });
    }
  };
};

