var fullWindow = new function () {

  //catch DOM
  var $height;
  var $width;
  var $objects;

  //bind events
  $(document).ready(function () {
    $objects = $('.fullWindow');

    changeHeight();
  });

  $(window).bind('resize orientationchange', function () {
    if($width == $(window).width()) {
      return true;
    }

    changeHeight();
  });

  //private functions
  var changeHeight = function () {
    $height = $(window).height();
    $width = $(window).width();
    $objects.css({'height': $height});
  };
};